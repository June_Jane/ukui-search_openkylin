<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>UkuiSearch::BestListWidget</name>
    <message>
        <source>Best Matches</source>
        <translation>ئەڭ ياخشى ماسلاشتۇرۇش</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::CreateIndexAskDialog</name>
    <message>
        <source>ukui-search</source>
        <translation>ئىزدەش ukui</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>ئىزدەش</translation>
    </message>
    <message>
        <source>No</source>
        <translation>ئەمەلدىن قالتۇرۇش</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>جەزملەشتۈرۈش</translation>
    </message>
    <message>
        <source>close</source>
        <translation>يېپىش</translation>
    </message>
    <message>
        <source>Creating index can help you get results more quickly. Would you like to create one?</source>
        <translation>ئىندېكس بەرپا قىلىش تېخىمۇ تېز نەتىجىگە ئېرىشىشىڭىزگە ياردەم بېرىدۇ. بىرنى بەرپا قىلىش قىلامسىز؟</translation>
    </message>
    <message>
        <source>Don&apos;t remind me again</source>
        <translation>ماڭا قايتا ئەسكەرتمەڭ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MainWindow</name>
    <message>
        <source>ukui-search</source>
        <translation>ئىزدەش ukui</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchLineEdit</name>
    <message>
        <source>Search</source>
        <translation>ئىزدەش</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchGui</name>
    <message>
        <source>Quit ukui-search application</source>
        <translation>ئىزدەش ئەپتىن چېكىنىش ukui</translation>
    </message>
    <message>
        <source>Show main window</source>
        <translation>ئاساسىي كۆزنەكنى كۆرسىتىش</translation>
    </message>
    <message>
        <source>unregister a plugin with &lt;pluginName&gt;</source>
        <translation>نى ئىشلىتىش &lt;pluginName&gt; تىزىمدىن ئۆچۈرۈش قىستۇرما دېتالى</translation>
    </message>
    <message>
        <source>register a plugin with &lt;pluginName&gt;</source>
        <translation>نى ئىشلىتىش &lt;pluginName&gt; تىزىملىتىش قىستۇرما دېتالى</translation>
    </message>
    <message>
        <source>move &lt;pluginName&gt; to the target pos</source>
        <translation>نى نىشانلىق ئورۇنغا يۆتكەش &lt;pluginName&gt;</translation>
    </message>
    <message>
        <source>move plugin to &lt;index&gt;</source>
        <translation>غا يۆتكەش &lt;index&gt; قىستۇرما دېتالنى</translation>
    </message>
</context>
<context>
    <name>search</name>
    <message>
        <source>search</source>
        <translation>ئىزدەش</translation>
    </message>
</context>
</TS>
