<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>UkuiSearch::BestListWidget</name>
    <message>
        <source>Best Matches</source>
        <translation>ەڭ جاقسى سايكەستىرۋ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::CreateIndexAskDialog</name>
    <message>
        <source>ukui-search</source>
        <translation>ٸزدەۋ ukui</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>ٸزدەۋ</translation>
    </message>
    <message>
        <source>close</source>
        <translation>جابۋ</translation>
    </message>
    <message>
        <source>Creating index can help you get results more quickly. Would you like to create one?</source>
        <translation>كورسەتكىش تاقپٸرلاۋ ەتۋ الٸدە تەز ناتيجەگە ئېرىشىشىڭىزگە جاردەم بەرەدٸ. بٸرٸن تاقپٸرلاۋ ەتۋ ورىندايسىزبا؟</translation>
    </message>
    <message>
        <source>Don&apos;t remind me again</source>
        <translation>جۇرەيىك قاتە ەسكەرتپە</translation>
    </message>
    <message>
        <source>No</source>
        <translation>كۇشىنەن قالتۇرۇش</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>تۇراقتاندىرۋ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MainWindow</name>
    <message>
        <source>ukui-search</source>
        <translation>ٸزدەۋ ukui</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchLineEdit</name>
    <message>
        <source>Search</source>
        <translation>ٸزدەۋ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchGui</name>
    <message>
        <source>Quit ukui-search application</source>
        <translation>ٸزدەۋ ئەپتىن شەگىنۋ ukui</translation>
    </message>
    <message>
        <source>Show main window</source>
        <translation>نەگٸزگٸ تەرەزەسى كورسەتۋ</translation>
    </message>
    <message>
        <source>unregister a plugin with &lt;pluginName&gt;</source>
        <translation>نى ٸستەتۋ &lt;pluginName&gt; تٸزٸمدەن ٴوشىرۋ قىستىرما دەتالى</translation>
    </message>
    <message>
        <source>register a plugin with &lt;pluginName&gt;</source>
        <translation>نى ٸستەتۋ &lt;pluginName&gt; تٸزٸمدەۇ قىستىرما دەتالى</translation>
    </message>
    <message>
        <source>move &lt;pluginName&gt; to the target pos</source>
        <translation>نى نىسانالىق ورٸنعا جوتكەۋ &lt;pluginName&gt;</translation>
    </message>
    <message>
        <source>move plugin to &lt;index&gt;</source>
        <translation>عا جوتكەۋ &lt;index&gt; قىستىرما دەتالدى</translation>
    </message>
</context>
<context>
    <name>search</name>
    <message>
        <source>search</source>
        <translation>ىزدە</translation>
    </message>
</context>
</TS>
