<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>UkuiSearch::BestListWidget</name>
    <message>
        <source>Best Matches</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::CreateIndexAskDialog</name>
    <message>
        <source>ukui-search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Creating index can help you get results more quickly. Would you like to create one?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Don&apos;t remind me again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MainWindow</name>
    <message>
        <source>ukui-search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchLineEdit</name>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchGui</name>
    <message>
        <source>Quit ukui-search application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>unregister a plugin with &lt;pluginName&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>register a plugin with &lt;pluginName&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>move &lt;pluginName&gt; to the target pos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>move plugin to &lt;index&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>search</name>
    <message>
        <source>search</source>
        <translation>suchen</translation>
    </message>
</context>
</TS>
