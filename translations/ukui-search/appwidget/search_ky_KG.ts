<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>UkuiSearch::BestListWidget</name>
    <message>
        <source>Best Matches</source>
        <translation>ەڭ جاقشى شايكەشتىرىپ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::CreateIndexAskDialog</name>
    <message>
        <source>ukui-search</source>
        <translation>ىزدۅۅ ukui</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>ىزدۅۅ</translation>
    </message>
    <message>
        <source>close</source>
        <translation>جابۇۇ</translation>
    </message>
    <message>
        <source>Creating index can help you get results more quickly. Would you like to create one?</source>
        <translation>بەلگىلەنگن نارق  پايدا قىلۇۇ جاسوو ،اتقارۇۇ  داعى ەلە تەز ناتىيجاعا ئېرىشىشىڭىزگە جەرلىك  بەرەت. بىردى پايدا قىلۇۇ جاسوو ،اتقارۇۇ  قىلاسىزبى؟</translation>
    </message>
    <message>
        <source>Don&apos;t remind me again</source>
        <translation>ماعا قايرا ەسكەرتبەڭ</translation>
    </message>
    <message>
        <source>No</source>
        <translation>ارعادان  قالتۇرۇش</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>بەكىتۉۉ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MainWindow</name>
    <message>
        <source>ukui-search</source>
        <translation>ىزدۅۅ ukui</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchLineEdit</name>
    <message>
        <source>Search</source>
        <translation>ىزدۅۅ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchGui</name>
    <message>
        <source>Quit ukui-search application</source>
        <translation>ىزدۅۅ ئەپتىن جانىش ، قايتىش  ukui</translation>
    </message>
    <message>
        <source>Show main window</source>
        <translation>نەگىزگى  كۅزۅنۅكتۉ كۅرسۅتۉۉ</translation>
    </message>
    <message>
        <source>unregister a plugin with &lt;pluginName&gt;</source>
        <translation>نى ىشتەتىش  &lt;pluginName&gt;  تىزىمدىكىتىن ۅچۉرۉۉ  قىستىرما تەتىگى</translation>
    </message>
    <message>
        <source>register a plugin with &lt;pluginName&gt;</source>
        <translation>نى ىشتەتىش  &lt;pluginName&gt; تىزىمدەتىش   قىستىرما تەتىگى</translation>
    </message>
    <message>
        <source>move &lt;pluginName&gt; to the target pos</source>
        <translation>نى نىشاندۇ ورۇنعا  جۅتكۅۅ &lt;pluginName&gt;</translation>
    </message>
    <message>
        <source>move plugin to &lt;index&gt;</source>
        <translation>عا  جۅتكۅۅ &lt;index&gt;  قىستىرما تەتىكتى</translation>
    </message>
</context>
<context>
    <name>search</name>
    <message>
        <source>search</source>
        <translation>ىزدۅۅ</translation>
    </message>
</context>
</TS>
