<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="140"/>
        <source>Content index incomplete.</source>
        <translation>مازمۇن   كۅرسۅتكۉچۉ تولۇق  ەمەس .</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-search-task.cpp" line="150"/>
        <source>Warning, Can not find home path.</source>
        <translation>دىققات، ۉي بۉلۅۅ جولۇن تاپقالى بولبويت .</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AiSearchPlugin</name>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="47"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="226"/>
        <source>Open</source>
        <translation>اچۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="48"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="227"/>
        <source>Open path</source>
        <translation>اچىق جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="49"/>
        <source>Copy Path</source>
        <translation>گۅچۉرۉش  جولۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="73"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="78"/>
        <source>AI Search</source>
        <translation>AI ىزدۅۅ</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="117"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="250"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="119"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="252"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>%1 نى اچىشقا  كۅڭۉلدۅگۉدۅي قولدونۇۇعا ەە بولوالبادى.</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="166"/>
        <source>File</source>
        <translation>ۅجۅت</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="204"/>
        <source>Path</source>
        <translation>جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="216"/>
        <source>Last time modified</source>
        <translation>سوڭعۇ ىرەت  ۅزگۅرتۉلگۅن</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="228"/>
        <source>Copy path</source>
        <translation>گۅچۉرۉش  جولۇ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppMatch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">Колдонмо сүрөттөлүшү:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchPlugin</name>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="31"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="248"/>
        <source>Open</source>
        <translation>اچۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="32"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="249"/>
        <source>Add Shortcut to Desktop</source>
        <translation>شىرە  ۉستۉنۅن قىسقارتۇۇ  قوشۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="250"/>
        <source>Add Shortcut to Panel</source>
        <translation>Panelگە قىسقارتۇۇ  قوشۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="34"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="251"/>
        <source>Install</source>
        <translation>قاچالوو</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="64"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="69"/>
        <source>Applications Search</source>
        <translation>پىروگىراممالاردى ىزدۅۅ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="154"/>
        <source>Application</source>
        <translation>ۅتۉنۉچ  جاسوو ،اتقارۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="199"/>
        <source>Application Description:</source>
        <translation>ۅتۉنۉچ   تۉشۉندۉرۉسۉ:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="55"/>
        <source>Application</source>
        <translation>ۅتۉنۉچ  جاسوو ،اتقارۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="60"/>
        <source>Application search.</source>
        <translation>قولدونۇشچان ىزدۅۅ.</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::DirSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="313"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="464"/>
        <source>Open</source>
        <translation>اچۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="314"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="465"/>
        <source>Open path</source>
        <translation>اچىق جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="315"/>
        <source>Copy Path</source>
        <translation>گۅچۉرۉش  جولۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="334"/>
        <source>Dir Search</source>
        <translation>ىزدۅۅ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="400"/>
        <source>Directory</source>
        <translation>مازمۇۇنۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="329"/>
        <source>Dir search.</source>
        <translation>ىزدۅۅ</translation>
    </message>
    <message>
        <source>directory</source>
        <translation type="vanished">目录</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="442"/>
        <source>Path</source>
        <translation>جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="454"/>
        <source>Last time modified</source>
        <translation>سوڭعۇ ىرەت  ۅزگۅرتۉلگۅن</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="466"/>
        <source>Copy path</source>
        <translation>گۅچۉرۉش  جولۇ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContengSearchPlugin</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Ачуу</translation>
    </message>
    <message>
        <source>Open path</source>
        <translation type="vanished">Ачык жол</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">Көчүрүү жолу</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">文本内容搜索</translation>
    </message>
    <message>
        <source>File content search.</source>
        <translation type="vanished">Файлдын мазмунун издөө.</translation>
    </message>
    <message>
        <source>File content search</source>
        <translation type="vanished">Файлдын мазмунун издөө</translation>
    </message>
    <message>
        <source>OCR</source>
        <translation type="vanished">ОКР</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Файл</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="vanished">Жол</translation>
    </message>
    <message>
        <source>Last time modified</source>
        <translation type="vanished">Акыркы жолу өзгөртүлүп берилди</translation>
    </message>
    <message>
        <source>Copy path</source>
        <translation type="vanished">Көчүрмө жолу</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="510"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="760"/>
        <source>Open</source>
        <translation>اچۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="511"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="761"/>
        <source>Open path</source>
        <translation>اچىق جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="512"/>
        <source>Copy Path</source>
        <translation>گۅچۉرۉش  جولۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="535"/>
        <source>File content search.</source>
        <translation>ۅجۅت مازمۇنۇن ىزدۅۅ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="540"/>
        <source>File content search</source>
        <translation>ۅجۅت مازمۇنۇن ىزدۅۅ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="624"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="628"/>
        <source>File</source>
        <translation>ۅجۅت</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="738"/>
        <source>Path</source>
        <translation>جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="750"/>
        <source>Last time modified</source>
        <translation>سوڭعۇ ىرەت  ۅزگۅرتۉلگۅن</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="762"/>
        <source>Copy path</source>
        <translation>گۅچۉرۉش  جولۇ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="63"/>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="89"/>
        <source>File Content</source>
        <translation>ۅجۅت مازمۇنۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="68"/>
        <source>File Content Search</source>
        <translation>ۅجۅت مازمۇن ىزدۅۅ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="91"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="250"/>
        <source>Open</source>
        <translation>اچۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="92"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="251"/>
        <source>Open path</source>
        <translation>اچىق جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="93"/>
        <source>Copy Path</source>
        <translation>گۅچۉرۉش  جولۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="112"/>
        <source>File Search</source>
        <translation>ۅجۅت ىزدۅۅ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="107"/>
        <source>File search.</source>
        <translation>ۅجۅت ىزدۅۅ.</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Ооба</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="155"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="275"/>
        <source>ukui-search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="156"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="276"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="158"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="278"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>%1 نى اچىشقا  كۅڭۉلدۅگۉدۅي قولدونۇۇعا ەە بولوالبادى.</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="186"/>
        <source>File</source>
        <translation>ۅجۅت</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="228"/>
        <source>Path</source>
        <translation>جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="240"/>
        <source>Last time modified</source>
        <translation>سوڭعۇ ىرەت  ۅزگۅرتۉلگۅن</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="252"/>
        <source>Copy path</source>
        <translation>گۅچۉرۉش  جولۇ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearch</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="357"/>
        <source>From</source>
        <translation>اندان</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="358"/>
        <source>Time</source>
        <translation>ۇباقىتتا</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="359"/>
        <source>To</source>
        <translation>عا</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="360"/>
        <source>Cc</source>
        <translation>cc</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearchPlugin</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="48"/>
        <source>open</source>
        <translation>اچۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="57"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="62"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="67"/>
        <source>Mail Search</source>
        <translation>ەلەكتىروندۇق جولدونمو  ىزدۅۅ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="114"/>
        <source>Mail</source>
        <translation>پوچتو جولدونموسۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="251"/>
        <source>Open</source>
        <translation>اچۇۇ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearch</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="243"/>
        <source>Note Description:</source>
        <translatorcomment>便签内容：</translatorcomment>
        <translation>ەسكەرتۉۉ:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearchPlugin</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="178"/>
        <source>Open</source>
        <translatorcomment>打开</translatorcomment>
        <translation>اچۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="52"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="156"/>
        <source>Note Search</source>
        <translatorcomment>便签</translatorcomment>
        <translation>ەسكەرتۉۉ ىزدۅۅ</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="47"/>
        <source>Note Search.</source>
        <translatorcomment>便签.</translatorcomment>
        <translation>ەسكەرتۉۉ ىزدۅۅ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="130"/>
        <source>Application</source>
        <translatorcomment>应用</translatorcomment>
        <translation>ۅتۉنۉچ  جاسوو ،اتقارۇۇ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchManager</name>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="72"/>
        <source>Path:</source>
        <translation>جول:</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="73"/>
        <source>Modified time:</source>
        <translation>ۅزگۅرتۉلگۅن ۇباقتى:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchResultPropertyInfo</name>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="44"/>
        <source>file path</source>
        <translation>ۅجۅت جولۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="50"/>
        <source>file name</source>
        <translation>ۅجۅت ناامى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="56"/>
        <source>file icon name</source>
        <translation>ۅجۅت سىن بەلگىسى  ناامى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="62"/>
        <source>modified time</source>
        <translation>ۅزگۅرتۉلگۅن ۇباقىت</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="68"/>
        <source>application desktop file path</source>
        <translation>قولدونۇشچان ئۈستەلئۈستى ۅجۅت جولۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="74"/>
        <source>application local name</source>
        <translation>پراگرامما جەردىك ناامى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="80"/>
        <source>application icon name</source>
        <translation>پراگرامما سىن بەلگىسى  ناامى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="86"/>
        <source>application description</source>
        <translation>ۅتۉنۉچ  تۉشۅندۉرۉش</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="92"/>
        <source>is online application</source>
        <translation>توردو ۅتۉنۉچ  جاسوو ،اتقارۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="98"/>
        <source>application package name</source>
        <translation>پراگرامما بوقچوسۇ ناامى</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchTaskPluginManager</name>
    <message>
        <source>plugin type: %1, is disabled!</source>
        <translation type="vanished">плагин түрү: %1, майып!</translation>
    </message>
    <message>
        <source>plugin type: %1, is not registered!</source>
        <translation type="vanished">плагин түрү: %1, катталган эмес!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsSearchPlugin</name>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="37"/>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="177"/>
        <source>Open</source>
        <translation>اچۇۇ</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="57"/>
        <source>Settings Search</source>
        <translation>تەڭشەك ىزدۅۅ</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="52"/>
        <source>Settings search.</source>
        <translation>تەڭشەك ىزدۅۅ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="166"/>
        <source>Settings</source>
        <translation>تەڭشەكتەر</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/ukui-search-task.cpp" line="129"/>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation>گەزەكتەكى  مىلدەت UUID  قاتالىعى كۅرۉنۉشتۅرۉ تىزىملىتالمىغان  قىستىرما ىشتەتىلەت!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTaskPrivate</name>
    <message>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation type="vanished">Учурдагы тапшырма уид катасы же каттоодон өткөн плагин колдонулат!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchPlugin</name>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="46"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="190"/>
        <source>Start browser search</source>
        <translation>تور  كۅرگۉچ ىزدەشتى  باشتوو</translation>
    </message>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="58"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="63"/>
        <source>Web Page</source>
        <translation>تور  بەت</translation>
    </message>
</context>
</TS>
