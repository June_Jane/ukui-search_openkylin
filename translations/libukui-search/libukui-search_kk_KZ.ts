<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="140"/>
        <source>Content index incomplete.</source>
        <translation>مازمۇن كورسەتكىش تولٸق ەمەس.</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-search-task.cpp" line="150"/>
        <source>Warning, Can not find home path.</source>
        <translation>ديقات، وتباسى جولىن تاپقالٸ بولمايدى.</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AiSearchPlugin</name>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="47"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="226"/>
        <source>Open</source>
        <translation>ٸشٸۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="48"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="227"/>
        <source>Open path</source>
        <translation>ايقٸن جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="49"/>
        <source>Copy Path</source>
        <translation>كوشىرۋ جولى</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="73"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="78"/>
        <source>AI Search</source>
        <translation>AI ٸزدەۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="117"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="250"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="119"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="252"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>%1 نى اشۋعا كوڭىلگىدەي قولدانۋعا قول جەتكٸزبەدٸ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="166"/>
        <source>File</source>
        <translation>حۇجات</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="204"/>
        <source>Path</source>
        <translation>جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="216"/>
        <source>Last time modified</source>
        <translation>سوڭعٸ رەت وزگەرتىلگەن</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="228"/>
        <source>Copy path</source>
        <translation>كوشىرۋ جولى</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppMatch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">Өтінім сипаттамасы:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchPlugin</name>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="31"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="248"/>
        <source>Open</source>
        <translation>ٸشٸۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="32"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="249"/>
        <source>Add Shortcut to Desktop</source>
        <translation>ۇستەل ٷستٸنەن قىسقارتۋ قوسۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="250"/>
        <source>Add Shortcut to Panel</source>
        <translation>Panelگە قىسقارتۋ قوسۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="34"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="251"/>
        <source>Install</source>
        <translation>تۇسىرۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="64"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="69"/>
        <source>Applications Search</source>
        <translation>پرٶگراممالاردى ٸزدەۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="154"/>
        <source>Application</source>
        <translation>جابلماس ەتۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="199"/>
        <source>Application Description:</source>
        <translation>جابلماس تۇسٸندٸرۋشٸ:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="55"/>
        <source>Application</source>
        <translation>جابلماس ەتۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="60"/>
        <source>Application search.</source>
        <translation>قولدانعىش ٸزدەۋ.</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::DirSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="313"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="464"/>
        <source>Open</source>
        <translation>ٸشٸۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="314"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="465"/>
        <source>Open path</source>
        <translation>ايقٸن جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="315"/>
        <source>Copy Path</source>
        <translation>كوشىرۋ جولى</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="334"/>
        <source>Dir Search</source>
        <translation>دۇرا ٸزدەۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="400"/>
        <source>Directory</source>
        <translation>باسمازمۇن</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="329"/>
        <source>Dir search.</source>
        <translation>ٸزدەۋ</translation>
    </message>
    <message>
        <source>directory</source>
        <translation type="vanished">目录</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="442"/>
        <source>Path</source>
        <translation>جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="454"/>
        <source>Last time modified</source>
        <translation>سوڭعٸ رەت وزگەرتىلگەن</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="466"/>
        <source>Copy path</source>
        <translation>كوشىرۋ جولى</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContengSearchPlugin</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Ашу</translation>
    </message>
    <message>
        <source>Open path</source>
        <translation type="vanished">Жолды ашу</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">Жолды көшіру</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">文本内容搜索</translation>
    </message>
    <message>
        <source>File content search.</source>
        <translation type="vanished">Файл мазмұнын іздеу.</translation>
    </message>
    <message>
        <source>File content search</source>
        <translation type="vanished">Файл мазмұнын іздеу</translation>
    </message>
    <message>
        <source>OCR</source>
        <translation type="vanished">OCR</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Файл</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="vanished">Жол</translation>
    </message>
    <message>
        <source>Last time modified</source>
        <translation type="vanished">Соңғы рет өзгертілген</translation>
    </message>
    <message>
        <source>Copy path</source>
        <translation type="vanished">Көшіру жолы</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="510"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="760"/>
        <source>Open</source>
        <translation>ٸشٸۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="511"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="761"/>
        <source>Open path</source>
        <translation>ايقٸن جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="512"/>
        <source>Copy Path</source>
        <translation>كوشىرۋ جولى</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="535"/>
        <source>File content search.</source>
        <translation>حۇجات مازمۇندى ٸزدەۋ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="540"/>
        <source>File content search</source>
        <translation>حۇجات مازمۇندى ٸزدەۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="624"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="628"/>
        <source>File</source>
        <translation>حۇجات</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="738"/>
        <source>Path</source>
        <translation>جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="750"/>
        <source>Last time modified</source>
        <translation>سوڭعٸ رەت وزگەرتىلگەن</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="762"/>
        <source>Copy path</source>
        <translation>كوشىرۋ جولى</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="63"/>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="89"/>
        <source>File Content</source>
        <translation>حۇجات مازمۇنى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="68"/>
        <source>File Content Search</source>
        <translation>حۇجات مازمۇن ٸزدەۋ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="91"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="250"/>
        <source>Open</source>
        <translation>ٸشٸۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="92"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="251"/>
        <source>Open path</source>
        <translation>ايقٸن جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="93"/>
        <source>Copy Path</source>
        <translation>كوشىرۋ جولى</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="112"/>
        <source>File Search</source>
        <translation>حۇجات ٸزدەۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="107"/>
        <source>File search.</source>
        <translation>حۇجات ٸزدەۋ.</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Иә</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="155"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="275"/>
        <source>ukui-search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="156"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="276"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="158"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="278"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>%1 نى اشۋعا كوڭىلگىدەي قولدانۋعا قول جەتكٸزبەدٸ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="186"/>
        <source>File</source>
        <translation>حۇجات</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="228"/>
        <source>Path</source>
        <translation>جول</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="240"/>
        <source>Last time modified</source>
        <translation>سوڭعٸ رەت وزگەرتىلگەن</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="252"/>
        <source>Copy path</source>
        <translation>كوشىرۋ جولى</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearch</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="357"/>
        <source>From</source>
        <translation>وننان</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="358"/>
        <source>Time</source>
        <translation>ۋاقىتىندا</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="359"/>
        <source>To</source>
        <translation>عا</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="360"/>
        <source>Cc</source>
        <translation>cc</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearchPlugin</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="48"/>
        <source>open</source>
        <translation>ٸشٸۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="57"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="62"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="67"/>
        <source>Mail Search</source>
        <translation>ەلەكتروندٸق حات ٸزدەۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="114"/>
        <source>Mail</source>
        <translation>پوچتا جولدانباسى</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="251"/>
        <source>Open</source>
        <translation>ٸشٸۋ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearch</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="243"/>
        <source>Note Description:</source>
        <translatorcomment>便签内容：</translatorcomment>
        <translation>ەسكەرتپەۋ:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearchPlugin</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="178"/>
        <source>Open</source>
        <translatorcomment>打开</translatorcomment>
        <translation>ٸشٸۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="52"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="156"/>
        <source>Note Search</source>
        <translatorcomment>便签</translatorcomment>
        <translation>ەسكەرتپەۋ ٸزدەۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="47"/>
        <source>Note Search.</source>
        <translatorcomment>便签.</translatorcomment>
        <translation>ەسكەرتپەۋ ىزدە.</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="130"/>
        <source>Application</source>
        <translatorcomment>应用</translatorcomment>
        <translation>جابلماس ەتۋ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchManager</name>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="72"/>
        <source>Path:</source>
        <translation>جول:</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="73"/>
        <source>Modified time:</source>
        <translation>وزگەرتىلگەن ۋاقىتى:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchResultPropertyInfo</name>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="44"/>
        <source>file path</source>
        <translation>حۇجات جولى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="50"/>
        <source>file name</source>
        <translation>حۇجات مى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="56"/>
        <source>file icon name</source>
        <translation>حۇجات سىن بەلگىسى مى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="62"/>
        <source>modified time</source>
        <translation>وزگەرتىلگەن ۋاقىت</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="68"/>
        <source>application desktop file path</source>
        <translation>قولدانعىش ئۈستەلئۈستى حۇجات جولى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="74"/>
        <source>application local name</source>
        <translation>پروگرامما جەرلىك مى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="80"/>
        <source>application icon name</source>
        <translation>پروگرامما سىن بەلگىسى مى</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="86"/>
        <source>application description</source>
        <translation>جابلماس تۇسىندىرۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="92"/>
        <source>is online application</source>
        <translation>توردا جابلماس ەتۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="98"/>
        <source>application package name</source>
        <translation>پروگرامما بوقشاسٸ مى</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchTaskPluginManager</name>
    <message>
        <source>plugin type: %1, is disabled!</source>
        <translation type="vanished">плагин түрі:% 1, өшірілген!</translation>
    </message>
    <message>
        <source>plugin type: %1, is not registered!</source>
        <translation type="vanished">плагин түрі:% 1, тіркелмеген!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsSearchPlugin</name>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="37"/>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="177"/>
        <source>Open</source>
        <translation>ٸشٸۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="57"/>
        <source>Settings Search</source>
        <translation>تەڭشە ٸزدەۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="52"/>
        <source>Settings search.</source>
        <translation>تەڭشە ٸزدەۋ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="166"/>
        <source>Settings</source>
        <translation>تەڭشەۋلەر</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/ukui-search-task.cpp" line="129"/>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation>كەزەكتەگى مىندەتتى UUID قاتەلىگى ياكي تىزىملىتالمىغان قىستىرما ٸستەتەدٸ!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTaskPrivate</name>
    <message>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation type="vanished">Ағымдағы тапсырма uuid қатесі немесе тіркелмеген плагин қолданылады!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchPlugin</name>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="46"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="190"/>
        <source>Start browser search</source>
        <translation>تور شولىعىش ىزدەۋدى باستاۋ</translation>
    </message>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="58"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="63"/>
        <source>Web Page</source>
        <translation>تور شاراپات</translation>
    </message>
</context>
</TS>
