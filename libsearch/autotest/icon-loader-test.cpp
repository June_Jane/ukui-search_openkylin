/*
*
 * Copyright (C) 2024, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */

#include "icon-loader-test.h"
#include <QTest>
#include <icon-loader.h>
#include <QDebug>

void IconLoaderTest::testLoadIconQt()
{
    QIcon icon = UkuiSearch::IconLoader::loadIconQt("noIcon", QIcon("kylin-search"));
    QCOMPARE(icon.isNull(), false);
    icon = UkuiSearch::IconLoader::loadIconQt("kylin-search");
    QCOMPARE(icon.isNull(), false);
}
QTEST_MAIN(IconLoaderTest)